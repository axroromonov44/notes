# KEEP NOTES - FLUTTER

## Hive NoSql - Flutter Bloc 

### Yotube Channel
[Frave Developer](https://www.youtube.com/channel/UCkNYlmbx487MPmYvfSMAdRg)

### Video Yotube
[Flutter - Keep Notes](https://www.youtube.com/watch?v=pPCfF87fsTU&t=72s&ab_channel=FraveDeveloper)

### Social Media

<a href="https://www.facebook.com/fraveDeveloper"><img src="https://github.com/aritraroy/social-icons/blob/master/facebook-icon.png?raw=true" width="50"></a>
<a href="https://www.instagram.com/frave_developer"><img src="https://github.com/aritraroy/social-icons/blob/master/instagram-icon.png?raw=true" width="50"></a>

### Donate

<a href="https://www.buymeacoffee.com/frave"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" width="100"></a>
<a href="https://www.paypal.me/Fpereza"><img src="https://img.flaticon.com/icons/png/512/888/888870.png" width="100"></a>

___

## Screenshot

<table border>
    <tr>
        <td><img src="./Screenshot/Screenshot_1627771490.png" alt="" width="200"></td>
        <td><img src="./Screenshot/Screenshot_1627771494.png" alt="" width="200"></td>
        <td><img src="./Screenshot/Screenshot_1627771498.png" alt="" width="200"></td>
    <tr>
</table>